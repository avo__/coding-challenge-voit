/**
 * Contains the same data as the database but in a more manageable format
 */
export interface ServiceDTO {
  key: string;
  intro: {
    de: string;
    en: string;
  };
  level: number;
  title: {
    de: string;
    en: string;
  };
}

export function convertToDTO(rawServices: any): ServiceDTO[] {
  // Type and data consistency checks and other error handling is omitted for now
  const serviceDTOs: ServiceDTO[] = [];
  const serviceKeys: string[] = Object.keys(rawServices);
  for (const serviceKey of serviceKeys) {
    serviceDTOs.push({
      key: serviceKey,
      title: rawServices[serviceKey].title,
      intro: rawServices[serviceKey].intro,
      level: rawServices[serviceKey].level,
    });
  }
  return serviceDTOs;
}
