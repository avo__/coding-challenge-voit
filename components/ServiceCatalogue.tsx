import React, {useEffect, useState} from 'react';
import {FlatList, Switch, View, StyleSheet} from 'react-native';
import {ServiceDTO} from '../converter';
import {serviceStore} from '../ServiceStore';
import ServiceCatalogueEntry from './ServiceCatalogueEntry';

export default () => {
  const [allServices, setAllServices] = useState<ServiceDTO[]>([]);
  const [services, setServices] = useState<ServiceDTO[]>([]);
  const [refreshing, setRefreshing] = useState(false);
  const [switchValue, setSwitchValue] = useState(false);

  const toggleSwitch = (value: boolean) => {
    setSwitchValue(value);
    applyFilter();
  };

  function applyFilter() {
    if (switchValue) {
      setServices(allServices);
    } else {
      setServices(
        allServices.filter((service) => {
          return service.level >= 2;
        }),
      );
    }
  }

  useEffect(() => {
    serviceStore.findServices().then((updatedServices) => {
      setAllServices(updatedServices);
      setServices(updatedServices);
    });
  }, []);

  return (
    <View style={styles.root}>
      <View style={styles.footer}>
        <Switch value={switchValue} onValueChange={toggleSwitch} />
      </View>
      <FlatList
        data={services}
        keyExtractor={(item) => item.key}
        renderItem={({item}) => <ServiceCatalogueEntry service={item} />}
        refreshing={refreshing}
        onRefresh={() => {
          setRefreshing(true);
          serviceStore.findServices().then((updatedServices) => {
            setAllServices(updatedServices);
            setRefreshing(false);
            applyFilter();
          });
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    justifyContent: 'space-between',
  },
  footer: {
    alignSelf: 'flex-end',
  },
});
