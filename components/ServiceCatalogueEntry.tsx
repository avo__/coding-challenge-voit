import {ServiceDTO} from '../converter';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {serviceStore} from '../ServiceStore';

export default (props: {service: ServiceDTO}) => {
  const [expand, setExpand] = useState(false);
  const [iconUrl, setIconUrl] = useState<string | null>(null);

  useEffect(() => {
    serviceStore.findIconUrl(props.service.key).then((url) => {
      setIconUrl(url);
    });
  }, []);

  return (
    <TouchableOpacity onPress={() => setExpand(!expand)}>
      <View style={styles.root}>
        <View style={styles.content}>
          {iconUrl && <Image style={styles.icon} source={{uri: iconUrl}} />}
          <Text style={styles.title} numberOfLines={2}>
            {props.service.title.de}
          </Text>
          {expandIndicator()}
        </View>
        {description()}
      </View>
    </TouchableOpacity>
  );

  function description() {
    if (expand) {
      return (
        <View style={styles.description}>
          <Text style={styles.descriptionText}>{props.service.intro.en}</Text>
          {/*this is not part of the mockup, but I thought it makes it easier to check if it is working as intended*/}
          <Text>{`(${props.service.level})`}</Text>
        </View>
      );
    }
  }

  function expandIndicator() {
    // This is pretty ugly, a icon should be used for this
    return <Text>{expand ? 'V' : '>'}</Text>;
  }
};

const styles = StyleSheet.create({
  root: {
    marginTop: 15,
    padding: 15,
    marginHorizontal: 10,
    borderWidth: 0.1,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    paddingRight: 16,
  },
  description: {
    marginTop: 10,
    fontSize: 10,
    padding: 5,
  },
  descriptionText: {
    color: 'gray',
  },
  icon: {
    width: 60,
    height: 60,
  },
});
