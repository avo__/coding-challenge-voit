# NUI coding challenge

Hi Tobias, 
normally I would not commit a file like this, but since this task is about you getting an idea how I work, I thought it would
make sense to include my notes and show my train of thought while implementing the task. 
I will also try to reflect this in the commit history a refrain from squashing and "cleaning" it at the end.

## Subtasks 
### T1: Project setup (done)
* make sure all resources are accessible 
* dependencies and environment is setup
* app can be built and started 

### T2: Access Firebase (done)
* Load service data from backend 
* Display as list

### T3: Filters (done)
* filter for level >= 2

### T4: Icons (done)
* Access storage and load icons for services
