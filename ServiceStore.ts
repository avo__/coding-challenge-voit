import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import {convertToDTO, ServiceDTO} from './converter';

class ServiceStore {
  private FIREBASE_DB_URL =
    'https://nui-testchallenge-default-rtdb.europe-west1.firebasedatabase.app/services';
  private STORAGE_BASE_URL = 'gs://nui-testchallenge.appspot.com/assets';
  private db = database();

  public async findServices(): Promise<ServiceDTO[]> {
    const servicesSnapShot = await this.db
      .refFromURL(this.FIREBASE_DB_URL)
      .once('value');
    servicesSnapShot.val();
    return convertToDTO(servicesSnapShot.val());
  }

  public async findIconUrl(key: string): Promise<string> {
    return await storage()
      .refFromURL(`${this.STORAGE_BASE_URL}/${key}.png`)
      .getDownloadURL();
  }
}

export const serviceStore = new ServiceStore();
