/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';

import ServiceCatalogue from './components/ServiceCatalogue';

const App = () => {
  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.root}>
        <ServiceCatalogue />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#ededed',
  },
});

export default App;
